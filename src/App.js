import React, { useReducer } from "react";
import "./App.css";
import Button from "./components/Button";
import OperationBtn from "./components/OperationBtn";
import BtnPersent from "./components/BtnPersent";
import Delete from "./Icons/delete.png";

export const ACT = {
  addDigit: "addDigit",
  clear: "clear",
  del: "del",
  operation: "operation",
  equal: "equal",
  negative: "negative",
};

//handel button
function reducer(state, { type, payload }) {
  // eslint-disable-next-line default-case
  switch (type) {
    //handle input nilai
    case ACT.addDigit:
      if (payload.digit === "0" && state.curOperand === "0") {
        return state;
      }
      if (payload.digit === "." && state.curOperand.includes(".")) {
        return state;
      }

      return {
        ...state,
        curOperand: `${state.curOperand || ""}${payload.digit}`,
      };
    //handle chose operation
    case ACT.operation:
      if (state.curOperand == null && state.prevOperand == null) {
        return state;
      }
      if (state.curOperand == null) {
        return {
          ...state,
          operation: payload.operation,
        };
      }
      if (state.prevOperand == null) {
        return {
          ...state,
          operation: payload.operation,
          prevOperand: state.curOperand,
          curOperand: null,
        };
      }
      return {
        ...state,
        prevOperand: calculate(state),
        operation: payload.operation,
        curOperand: null,
      };
    //handle C button
    case ACT.clear:
      return {};
    //handle equal button
    case ACT.equal:
      if (
        state.operation == null ||
        state.curOperand == null ||
        state.prevOperand == null
      ) {
        return state;
      } else {
        return {
          ...state,
          prevOperand: null,
          operation: null,
          curOperand: calculate(state),
        };
      }
    //handle delete button
    case ACT.del:
      if (state.curOperand == null) {
        return state;
      } else if (state.curOperand.length === 1) {
        return {
          ...state,
          curOperand: null,
          operation: null,
        };
      } else {
        return {
          ...state,
          curOperand: state.curOperand.slice(0, -1),
        };
      }
    case ACT.negative:
  }
}

// if(state.addDigit){
//   return{
//     ...state,
//     curOperand: payload.digit,
//     ovewrite: false,
//   }
// }

//handle calculate
function calculate({ curOperand, prevOperand, operation }) {
  const prev = parseFloat(prevOperand);
  const cur = parseFloat(curOperand);
  let result = "";

  if (isNaN(prev) || isNaN(cur)) {
    return "";
  }
  switch (operation) {
    case "+":
      result = prev + cur;
      break;
    case "-":
      result = prev - cur;
      break;
    case "/":
      result = prev / cur;
      break;
    case "x":
      result = prev * cur;
      break;
    case "%":
      result = prev * cur * 0.01;
      break;
    default:
  }
  return result.toString();
}

//

function App() {
  const [{ curOperand, prevOperand, operation }, dispatch] = useReducer(
    reducer,
    {}
  );

  const [theme, setTheme] = React.useState("light");

  return (
    //<Button/>
    <div className={`container container-${theme}`}>
      {/* Mode */}
      <div className="vertical-center">
      <button 
        style={{ marginTop: "15px" }}
        onClick={() => {
          setTheme((prevState) => {
            if (prevState === "light") {
              return "dark";
            } else {
              return "light";
            }
          });
        }}
      >
        Mode
      </button>
      </div>
      <div className="calculator-grid">
        <div className="output">
          <div className={`prev-operand font-prev-${theme}`}>
            {prevOperand} {operation}
          </div>
          <div className={`cur-operand font-cur-${theme}`}>{curOperand}</div>
        </div>

        <button className="btnUp" onClick={() => dispatch({ type: ACT.clear })}>
          C
        </button>

        <button className="btnUp">+/-</button>

        {/* operation */}
        <BtnPersent operation="%" dispatch={dispatch} />
        <OperationBtn operation="/" dispatch={dispatch} />

        <Button digit="7" dispatch={dispatch} />
        <Button digit="8" dispatch={dispatch} />
        <Button digit="9" dispatch={dispatch} />

        {/* operation */}
        <OperationBtn operation="x" dispatch={dispatch} />

        <Button digit="4" dispatch={dispatch} />
        <Button digit="5" dispatch={dispatch} />
        <Button digit="6" dispatch={dispatch} />

        {/* operation */}
        <OperationBtn operation="-" dispatch={dispatch} />

        <Button digit="1" dispatch={dispatch} />
        <Button digit="2" dispatch={dispatch} />
        <Button digit="3" dispatch={dispatch} />

        {/* operation */}
        <OperationBtn operation="+" dispatch={dispatch} />

        <Button digit="." dispatch={dispatch} />
        <Button digit="0" dispatch={dispatch} />
        <button onClick={() => dispatch({ type: ACT.del })}>
          <img className="img" src={Delete} alt="Delete" />
        </button>

        {/* equal */}
        <button
          className="operation"
          onClick={() => dispatch({ type: ACT.equal })}
        >
          =
        </button>
      </div>
      
    </div>
  );
}

export default App;
