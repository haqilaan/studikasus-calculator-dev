import {ACT} from "../App";


export default function Button({dispatch, digit}){
  return (
    <button 
      onClick={() => 
        dispatch({ type: ACT.addDigit, payload: {digit}})}
      >{digit}
    </button>
  )
}
