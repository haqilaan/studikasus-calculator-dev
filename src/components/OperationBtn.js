import {ACT} from "../App";


export default function OperationBtn({dispatch, operation}){
  return (
    <button className="operation"
      onClick={() => 
        dispatch({ type: ACT.operation, payload: {operation}})}
      >{operation}
    </button>
  )
}